from src import app, db
from flask_script import Manager, Server, Command

manager = Manager(app)

class CreateProducts(Command):

    def run(self):
        from src.models import Product

        db.session.add(Product(title="LG", clicks=0, kind=0))
        db.session.add(Product(title="Samsung", clicks=0, kind=0))
        db.session.add(Product(title="Sony", clicks=0, kind=0))
        db.session.add(Product(title="Ardo", clicks=0, kind=1))
        db.session.add(Product(title="Liebherr", clicks=0, kind=1))
        db.session.add(Product(title="Hitachi", clicks=0, kind=1))
        db.session.commit()

manager.add_command("create-products", CreateProducts)
manager.add_command("run", Server(host="127.0.0.1", port=8000))


if __name__ == '__main__':
    app.debug = True
    manager.run()
