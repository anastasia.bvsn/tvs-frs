import os
import binascii

from flask import Flask
from flask_sqlalchemy import SQLAlchemy


app = Flask('__name__')
app.config["SECRET_KEY"] = binascii.hexlify(os.urandom(24))
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///db.sqlite3"
app.static_folder = 'static'
db = SQLAlchemy(app)

from src.models import Product
from src.views import *

db.create_all()
