from flask import Flask, render_template, redirect, url_for, request, jsonify
from src import app, db

from src.models import Kind, Product


def fetch_products():
    return Product.query


@app.route('/', methods=['GET'])
def index():
    tvs = fetch_products().filter(Product.kind==Kind.TV)
    frs = fetch_products().filter(Product.kind==Kind.FR)

    return render_template('index.html', tvs=tvs, frs=frs)


@app.route('/sorted', methods=['GET'])
def sorted_page():
    tvs = fetch_products().filter(Product.kind==Kind.TV).order_by(Product.clicks.desc())
    frs = fetch_products().filter(Product.kind==Kind.FR).order_by(Product.clicks.desc())

    return render_template('sorted.html', tvs=tvs, frs=frs)


@app.route('/increment', methods=['POST'])
def sorted_ok():
    increment()
    return jsonify({'status':'OK'});


def increment():
    update = Product.query.filter_by(
        id=request.form['id'],
    ).update(
        {'clicks' : Product.clicks + 1},
    )
    db.session.commit()
    return update
