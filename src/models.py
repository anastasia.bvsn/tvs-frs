from src import db


class Kind(object):
    TV = 0
    FR = 1


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.Unicode(64))
    clicks = db.Column(db.SmallInteger)
    kind = db.Column(db.SmallInteger, default=Kind.TV)
