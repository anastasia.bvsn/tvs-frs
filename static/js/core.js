$(document).ready(function() {
  $(document).on('click', '.products', function(ev) {
    var clicks = ev.target.parentElement.querySelector('.clicks');
    clicks.innerHTML++;
    id = ev.target.getAttribute('data-id');

    $.ajax({
      url: '/increment',
      type: 'POST',
      data: {
        "id" : id
      },
      success: function(response) {
        console.log(response);
      },
      error: function(error) {
        console.log(error);
      }
    });
  });
});
